var path = require('path');
var webpack = require('webpack');
 
module.exports = {
    devtool: 'inline-source-map',

    entry: {
        app: './src/app.js',
        // vendor: ['pixi', 'p2', 'phaser']
    },

    output: {
         path: path.resolve(__dirname, 'dist'),
         filename: 'bundle.js'
    },

    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.DefinePlugin({
            VERSION: JSON.stringify("0.0.01"),
            ENVIRONMENT: JSON.stringify("local"),
            STAGE: JSON.stringify("development")
        })
    ],

    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel-loader'],
                include: path.join(__dirname, 'src')
            },

            {
            test: /\.sass$/,
            use: [
                'style-loader',
                {
                loader: 'css-loader',
                options: {
                    sourceMap: true
                }
                },
                {
                loader: "sass-loader",
                options: {
                    sourceMap: true,
                    indentedSyntax: true
                }
                }
            ]
            },

            {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader" // creates style nodes from JS strings
                }, {
                    loader: "css-loader" // translates CSS into CommonJS
                }, {
                    loader: "sass-loader" // compiles Sass to CSS
                }]
            },

            {
            test: /\.css$/,
            use: [
                'style-loader', 'css-loader'
            ]
            },

            { test: /\.png|jpg$/, loader: 'url-loader?name=images/[name].[ext]' },
            { test: /\.(svg|ttf|eot|woff(2)?)(\?[a-z0-9=&.]+)?$/, loader: 'url-loader?name=fonts/[name].[ext]' }
        ]
    }

}
