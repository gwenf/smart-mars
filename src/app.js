import setup from './gameSetup';

var game = new Phaser.Game(24*32, 17*32, Phaser.AUTO, document.getElementById('root'));
var GameSetup = setup(game, Client);
var Client = GameSetup.Client;
game.state.add('Game', GameSetup.Game);
game.state.start('Game');
